import { Component } from '@angular/core';
import { Input } from '@angular/core/src/metadata/directives';
import { BookmarksService } from '../bookmarks.service';
import { IBookmark } from '../bookmarks.model';

@Component({
  selector: 'ma-bookmark-remove',
  templateUrl: './bookmark-remove.component.html'
})
export class BookmarkRemoveComponent {
  @Input() item: IBookmark;

  constructor(public bs: BookmarksService) {
  }
}
