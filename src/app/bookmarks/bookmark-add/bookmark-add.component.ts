import { Component } from '@angular/core';
import { Input } from '@angular/core/src/metadata/directives';
import { BookmarksService } from '../bookmarks.service';
import { IBookmark } from '../bookmarks.model';

@Component({
  selector: 'ma-bookmark-add',
  templateUrl: './bookmark-add.component.html'
})
export class BookmarkAddComponent {
  @Input() item: IBookmark;

  constructor(public bs: BookmarksService) {
  }
}
