import { Injectable, EventEmitter } from '@angular/core';
import { IBookmark } from './bookmarks.model';

@Injectable()
export class BookmarksService {
  private bookmarks: IBookmark[] = [];
  changed = new EventEmitter<IBookmark[]>();

  constructor() {
    this.bookmarks = JSON.parse(localStorage.getItem('bookmarks') || '[]');
  }

  add(item: IBookmark): void {
    this.bookmarks.push(item);
    this.changed.emit(this.get());
    this.sync();
  }

  remove(id: number | string): void {
    this.bookmarks = this.bookmarks.filter(bookmark => bookmark.id !== id);
    this.changed.emit(this.get());
    this.sync();
  }

  has(id: number | string): boolean {
    return this.bookmarks.some(item => item.id === id);
  }

  get(): IBookmark[] {
    return [...this.bookmarks];
  }

  private sync() {
    localStorage.setItem('bookmarks', JSON.stringify(this.bookmarks));
  }

}
