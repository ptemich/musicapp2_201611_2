import { Directive, Input, OnInit, HostListener, HostBinding } from '@angular/core';
import { IBookmark } from './bookmarks.model';
import { BookmarksService } from './bookmarks.service';

@Directive({
  selector: '[maBookmark]'
})
export class BookmarkDirective implements OnInit {
  @Input('maBookmark') item: IBookmark;
  @HostBinding('class.bookmarked') isBookmarked: boolean;

  constructor(private bs: BookmarksService) {
  }

  ngOnInit(): void {
  }

  @HostListener('click')
  onClick() {
    this.bs.add(this.item);
    this.isBookmarked = true;
  }
}
