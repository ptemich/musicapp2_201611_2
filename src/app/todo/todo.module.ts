import { NgModule } from '@angular/core';
import { TodoComponent } from './todo.component';
import { TodoItemComponent } from './todo-item.component';
import { TodoFormComponent } from './todo-form.component';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { TodoService } from './todo.service';
import { HttpModule } from '@angular/http';

@NgModule({
  declarations: [
    TodoComponent,
    TodoItemComponent,
    TodoFormComponent
  ],
  exports: [
    TodoComponent
  ],
  providers: [
    TodoService
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ]
})
export class TodoModule {}
