import { Component, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'ma-todo-form',
  templateUrl: './todo-form.component.html'
})
export class TodoFormComponent {
  @Output() itemCreated = new EventEmitter<string>();
  text: string;

  onSubmit() {
    this.itemCreated.emit(this.text);
    this.text = '';
  }
}
