import { Component } from '@angular/core';
import { ITodo } from './todo.model';
import { TodoService } from './todo.service';

@Component({
  selector: 'ma-todo',
  templateUrl: './todo.component.html'
})
export class TodoComponent {
  todos: ITodo[] = [];
  todosCompleted = 0;

  constructor(private ts: TodoService) {
    this.todos = ts.todos;
    this.setCompleted();
    this.ts.itemsChanged.subscribe(todos => {
      this.todos = todos;
      this.setCompleted();
    });
  }

  onTodoCompleted(todo: ITodo, status: boolean): void {
    this.ts.toggleCompleted(todo, status);
    this.setCompleted();
  }

  onTodoRemove(todo: ITodo): void {
    this.ts.removeItem(todo.id);
  }

  createItem(text: string): void {
    this.ts.createItem(text);
  }

  private setCompleted() {
    this.todosCompleted = this.todos.filter(t => t.completed).length;
  }
}
