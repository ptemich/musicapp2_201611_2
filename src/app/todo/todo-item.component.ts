import { Component, Input, Output, EventEmitter, ViewEncapsulation } from '@angular/core';
import { ITodo } from './todo.model';

@Component({
  selector: 'ma-todo-item',
  templateUrl: './todo-item.component.html',
  styleUrls: ['./todo-item.component.scss']
})
export class TodoItemComponent {
  @Input() item: ITodo;
  @Output() completed: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() removed = new EventEmitter<void>();

  toggleTodo() {
    this.completed.emit(!this.item.completed);
  }

  onRemove(e: MouseEvent) {
    this.removed.emit();
    e.preventDefault();
  }
}
