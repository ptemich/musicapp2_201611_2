export interface ITodo {
  id: number | string;
  text: string;
  completed: boolean;
}
