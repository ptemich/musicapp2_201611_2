import { Injectable, EventEmitter } from '@angular/core';
import { ITodo } from './todo.model';
import { Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';

@Injectable()
export class TodoService {
  version = 1.01;
  todos: ITodo[] = [];
  itemsChanged = new EventEmitter<ITodo[]>();

  constructor(private http: Http) {
    this.http.get('http://localhost:3000/todos')
      .map(res => res.json())
      .subscribe(res => {
        this.todos = res;
        this.itemsChanged.emit(this.todos);
      });
  }

  createItem(text: string) {
    const id = Math.floor(Math.random() * 100000);
    const newItem = {id, text, completed: false};
    this.todos.push(newItem);
    this.itemsChanged.emit(this.todos);
    this.http.post('http://localhost:3000/todos', newItem)
      .subscribe(() => {
      }, () => this.remove(id));
  }

  toggleCompleted(todo: ITodo, status: boolean) {
    this.todos.forEach(t => {
      if (t.id === todo.id) {
        t.completed = status;
      }
    });
  }

  private remove(id: number|string) {
    this.todos = this.todos.filter(todo => todo.id !== id);
    this.itemsChanged.emit(this.todos);
  }

  removeItem(id: number|string) {
    let todoToDelete: ITodo;
    let todoToDeleteIndex: number;
    this.todos.forEach((todo, index) => {
      if (todo.id === id) {
        todoToDelete = todo;
        todoToDeleteIndex = index;
      }
    });
    this.remove(id);
    this.http.delete(`http://localhost:3000/todos/${id}`)
      .subscribe(() => {
      }, () => {
        this.todos.splice(todoToDeleteIndex, 0, todoToDelete);
        this.itemsChanged.emit(this.todos);
      });
  }
}
