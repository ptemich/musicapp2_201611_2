import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SpotifyComponent } from './spotify.component';
import { SpotifyService } from './spotify.service';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { InfiniteScrollModule } from 'angular2-infinite-scroll';
import { SpotifyPosterComponent } from './spotify-poster/spotify-poster.component';
import { BookmarksModule } from '../bookmarks/bookmarks.module';
import { SharedModule } from '../shared/shared.module';
import { AlbumDetailsComponent } from './album-details/album-details.component';
import { RouterModule } from '@angular/router';
import { TrackPlayerComponent } from './track-player/track-player.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    HttpModule,
    RouterModule,
    InfiniteScrollModule,
    BookmarksModule,
    SharedModule
  ],
  providers: [
    SpotifyService
  ],
  declarations: [
    SpotifyComponent,
    SpotifyPosterComponent,
    AlbumDetailsComponent,
    TrackPlayerComponent
  ]
})
export class SpotifyModule {
}
