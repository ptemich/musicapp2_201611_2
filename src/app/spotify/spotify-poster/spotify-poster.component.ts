import {
  Component, OnInit, Input, OnChanges, SimpleChanges, trigger, transition, style, animate,
  state
} from '@angular/core';
import { IAlbum } from '../spotify.model';

@Component({
  selector: 'ma-spotify-poster',
  templateUrl: './spotify-poster.component.html',
  styleUrls: ['./spotify-poster.component.scss'],
  animations: [
    trigger('hasAlbum', [
      state('exist', style({transform: 'translateX(0px) scale(1)'})),
      transition(':enter', [
        style({transform: 'translateY(-100px) scale(0)'}),
        animate(100)
      ]),
      transition(':leave', [
        animate(100, style({transform: 'translateY(-100px) scale(0)'}))
      ])
    ])
  ]
})
export class SpotifyPosterComponent implements OnInit, OnChanges {
  @Input() album: IAlbum;
  @Input() size: 'sm' | 'md' | 'lg';
  imgUrl: string;

  constructor() {
  }

  ngOnInit() {
  }

  ngOnChanges() {
    const sizesDict = {
      lg: 0,
      md: 1,
      sm: 2
    };
    const image = this.album.images[sizesDict[this.size] || 0];
    if (image) {
      this.imgUrl = image.url;
    }
  }

}
