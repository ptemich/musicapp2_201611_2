import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SpotifyService } from '../spotify.service';
import { IAlbumDetails } from '../spotify.model';

interface IAlbumDetailsParams {
  id: string;
}

@Component({
  selector: 'ma-album-details',
  templateUrl: './album-details.component.html',
  styleUrls: ['./album-details.component.scss']
})
export class AlbumDetailsComponent implements OnInit {
  album: IAlbumDetails;

  constructor(private route: ActivatedRoute,
              private spotify: SpotifyService) {
  }

  ngOnInit() {
    this.spotify.getAlbum(this.route.snapshot.params['id'])
      .subscribe(album => this.album = album);
  }
}
