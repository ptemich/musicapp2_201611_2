import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { SpotifyService } from './spotify.service';
import { IAlbum } from './spotify.model';
import { InfiniteScrollEvent } from 'angular2-infinite-scroll';
import { FormControl } from '@angular/forms';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import { BookmarksService } from '../bookmarks/bookmarks.service';
import { IBookmark } from '../bookmarks/bookmarks.model';

@Component({
  selector: 'ma-spotify',
  templateUrl: './spotify.component.html',
  styleUrls: ['./spotify.component.scss']
})
export class SpotifyComponent implements OnInit, AfterViewInit {
  query: string;
  albums: IAlbum[] = [];
  bookmarks: IBookmark[] = [];
  loading = false;
  @ViewChild('queryRef') queryRef: FormControl;
  private resultsPage = 0;

  constructor(private spotify: SpotifyService,
              private bs: BookmarksService) {
  }

  ngOnInit() {
    this.bookmarks = this.bs.get();
    this.bs.changed
      .subscribe(bookmarks => this.bookmarks = bookmarks);
  }

  ngAfterViewInit(): void {
    this.queryRef.valueChanges
      .debounceTime(500)
      .distinctUntilChanged()
      .subscribe(val => this.searchAlbums(true));
  }

  saveBookmark(item: IBookmark): void {
    this.bs.add(item);
  }

  isBookmarked(item: IBookmark): boolean {
    return this.bs.has(item.id);
  }

  searchAlbums(reset: boolean) {
    if (reset) {
      this.resultsPage = 0;
    }
    this.loading = true;
    this.spotify.searchAlbums(this.query, this.resultsPage)
      .subscribe(albums => {
        this.albums = (this.resultsPage)
          ? [...this.albums, ...albums]
          : albums;

        this.loading = false;
      });
  }

  onScroll(event: InfiniteScrollEvent) {
    this.resultsPage++;
    this.searchAlbums(false);
  }

}
