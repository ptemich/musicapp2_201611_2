import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs';
import { IAlbum, IAlbumDetails } from './spotify.model';

@Injectable()
export class SpotifyService {
  private apiUrl = 'https://api.spotify.com/v1/';

  constructor(private http: Http) {
  }

  searchAlbums(query: string, page = 0): Observable<IAlbum[]> {
    const url = `${this.apiUrl}search?type=album&q=${query}&offset=${page * 20}`;
    return this.http.get(url)
      .map(res => res.json().albums.items);
  }

  getAlbum(id: string): Observable<IAlbumDetails> {
    const url = `${this.apiUrl}albums/${id}`;
    return this.http.get(url)
      .map(res => res.json());
  }
}
