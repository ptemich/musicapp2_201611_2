import { Component, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { Input } from '@angular/core/src/metadata/directives';
import { ITrack } from '../spotify.model';

@Component({
  selector: 'ma-track-player',
  templateUrl: './track-player.component.html',
  styleUrls: ['./track-player.component.scss']
})
export class TrackPlayerComponent {
  @Input() track: ITrack;
  @ViewChild('audio') audioEl: ElementRef;

  @Input() set play(play: boolean) {
    play
      ? this.audioEl.nativeElement.play()
      : this.audioEl.nativeElement.pause();
  }

  @Output() playChange = new EventEmitter<boolean>();

  togglePlay(audioEl: HTMLAudioElement): void {
    audioEl.paused
      ? audioEl.play()
      : audioEl.pause();

    this.playChange.emit(!audioEl.paused);
  }
}
