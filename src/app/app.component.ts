import { Component } from '@angular/core';

interface IMenuItem {
  label: string;
  route: Array<string | number>;
  exact?: boolean;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app works!';
  menuItems: IMenuItem[] = [
    {route: [''], label: 'Todosy', exact: true},
    {route: ['spotify'], label: 'Spotify'},
    {route: ['contact'], label: 'Contact Us'}
  ];
}
