import { Route } from '@angular/router';
import { TodoComponent } from './todo/todo.component';
import { SpotifyComponent } from './spotify/spotify.component';
import { ContactComponent } from './contact/contact.component';
import { AlbumDetailsComponent } from './spotify/album-details/album-details.component';

export const routes: Route[] = [
  {path: '', component: TodoComponent},
  {path: 'spotify', component: SpotifyComponent},
  {path: 'spotify/:id', component: AlbumDetailsComponent},
  {path: 'contact', component: ContactComponent}
];
