import { browser } from 'protractor';
import { ContactPage } from './contact.po';

describe('contactPage', () => {
  let page: ContactPage;

  beforeEach(() => {
    page = new ContactPage();
  });

  it('displays "Name:" as a label', () => {
    page.navigateTo();
    expect(page.nameLabel.getText()).toBe('Name:');
    // page.nameInput.sendKeys('cd');
  });

  it('displays error when name input is invalid', () => {
    page.navigateTo();
    page.nameInput.sendKeys('as');
    browser.pause();
    expect(page.nameError.isDisplayed()).toBe(true);
  });
});
