import { browser, element, by } from 'protractor';

export class ContactPage {
  navigateTo() {
    return browser.get('/contact');
  }

  get nameLabel() {
    return element(by.css('[for=name]'));
  }

  get nameControl() {
    return element.all(by.css('.control')).first();
  }

  get nameInput() {
    return this.nameControl.element(by.name('name'));
  }

  get nameError() {
    return this.nameControl.element(by.css('.is-danger > span'));
  }
}
